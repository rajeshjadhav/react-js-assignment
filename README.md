# React JS - Assignment README

====== How to proceed?  ===========

- Go to "react-js-assignment" then fire this "npm install" command on cmd/bash 
- After go to "react-js-assignment/server/" then fire this "npm install" command on cmd/bash 
- After above steps the project setup is done.
- Then go to root dir which is "react-js-assignment" & fire command "npm run dev" , it will start the server

====== Packages Installed ===========

- bootstrap & react-bootstrap - used for front-end component library.
- concurrently - used for running multiple commands concurrently.
- history - used for managing session history.
- react - used for building interface.
- react-dom - used for working with dom .
- react-redux - used for binding react with redux.
- react-router-dom - used for DOM binding for react router.
- react-scripts - used for configuring create-react-app.
- redux - used as state management library.
- redux-thunk - used as a middleware. 

====== Project Folder Structure  ===========

- public
    - index.html - main index.html file
- server (it contains package.json , node.js server releated scripts & services)
    - package.json
- src
    - actions 
        - contains action creators which send data to store
    - components
        - contains the React Functional Components
    - containers
        - contains the React Class Based Components
    - reducers
        - contains reducer which tells how state will be changed
    - routers
        - defines the routes for web application
    - store
        - crates a store for web application
    - utils
        - contains utility functions
- index.css
- index.js
- package.json