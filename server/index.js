var express = require('express');
var app = express();

//app.use(express.static('services'));
//app.use(express.static('images'));
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.post('/login', function (req, res) {
    res.json({
        "status": "success",
        "data":
            { 
                "id": 1,
                "email": "test@test.com",
                "token": "eyJhbGciOiJIUzUxMiJ9" 
            }
    })
  })

app.listen(3001);

console.log("Server Running !");