const authentication = {
    SHOW_LOGIN : "SHOW_LOGIN",
    LOGIN_REQUEST : "LOGIN_REQUEST",
    LOGIN_SUCCESS : "LOGIN_SUCCESS",
    LOGIN_FAILED : "LOGIN_FAILED",    
    LOGOUT : "LOGOUT"
};

export default authentication;