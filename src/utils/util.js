
export const parseJSON = (response) => {
	return response.text()
	.then((res) => {
		return res ? JSON.parse(res) : {}
	})
	.catch((error)=>{
		console.log(error)
	});
}

