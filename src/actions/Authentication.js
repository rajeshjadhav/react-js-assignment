import authConstant from '../constants/authentication';
import history from '../routers/history';
import { parseJSON } from '../utils/util';

const { LOGIN_SUCCESS } = authConstant;

export const login = (payload) => {	
    return (dispatch) => {
        
        return fetch('http://localhost:3001/login',{
			method:'POST',
			headers:{
				'Content-Type': 'application/json'
			},
            body: JSON.stringify(payload)
		})		
		.then(parseJSON)		
        .then(json => {
			if(json.status=='success'){
				dispatch({
					type: LOGIN_SUCCESS,
					payload: json.data
				});
				localStorage.setItem('token',json.data.token);				
				history.push('/aboutus');
			}
			else{				
				history.push('/login');
			}
		})
		.catch(err => {
			history.push('/login');
		});
    }
}

export function logout() {
    return (dispatch) => {
        localStorage.clear();        
        history.push("/");
    }
}