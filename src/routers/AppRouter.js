import { Provider } from 'react-redux';
import { BrowserRouter, Redirect, Route, Link,Switch, NavLink,Router } from 'react-router-dom';
import Login from '../containers/login/';
import Home from '../containers/home/';
import React from 'react';
import store from '../store/configureStore';
import history from '../routers/history';
import PageNotFound from '../containers/pageNotFound/';

const AppRouter = () => (	
    <BrowserRouter>  
        <div>
            <Provider store={store}>
                <Router history={history}>
                <Switch>				
                    <Route exact path="/" component={Login} />                     
                    {/* <Route exact path="/pageNotFound" component={PageNotFound} />        */}
                    {/* <Route exact path="/" component={Home} />                                   */}
                    <Route path="/login" render={ props => {
                           return localStorage.token==undefined
                                ?
                                <Login {...props}/>
                                :
                                <Redirect to='/' />
                    }} />                    
                    <Home/>
                </Switch>
                </Router>
            </Provider>
        </div>
    </BrowserRouter>	
);

export default AppRouter;