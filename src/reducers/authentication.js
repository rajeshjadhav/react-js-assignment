import authConstant from '../constants/authentication';

const { LOGIN_SUCCESS } = authConstant;

const initialState = {};

const authentication = ( state = initialState, action) => {

    switch(action.type){

        case LOGIN_SUCCESS:
            return Object.assign({},state,{
                id:action.payload.id,
                email:action.payload.email,
                token:action.payload.token
            });           

        default : 
            return state;

    }
};

export default authentication;

