import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React from 'react';
import LoginComponent  from '../../components/login/';
import * as authActionCreator from '../../actions/Authentication';

class Login extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            emailError:'',
            passwordError:''
        }
        this.onChangeInput = this.onChangeInput.bind(this);   
        this.onHandleSubmit = this.onHandleSubmit.bind(this);   
    }

    isEmailValid(email){
        let atpos = email.indexOf("@");        
        let dotpos = email.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {            
            return false;
        }
        else{
            return true;
        }
    }

    onChangeInput(e){        
        this.setState({
            [e.target.name] : e.target.value
        });        
    }
    
    onHandleSubmit(e){
        e.preventDefault();        
        let emailValid = this.isEmailValid(this.state.email);
        
        let emailError = (emailValid==true) ? '' : "Invalid EmailID !" ;
        let passwordError = (this.state.password.length)>= 8 ? '' : "Password must be >= 8 characters !";
        
        if( emailError=='' && passwordError==''){
            this.props.authAction.login({
                email:this.state.email,
                password:this.state.password
            });
        }
        else{
            this.setState({ emailError :emailError,passwordError:passwordError });    
        }
    }

    render(){
        return (
            <LoginComponent 
                onChangeInput = { this.onChangeInput }
                onHandleSubmit = { this.onHandleSubmit }
                emailError = { this.state.emailError }
                passwordError = { this.state.passwordError } />            
        )
    }
    
}

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({
    authAction : bindActionCreators(authActionCreator,dispatch)	
});

//
export default connect(mapStateToProps,mapDispatchToProps)(Login);