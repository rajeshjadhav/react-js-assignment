import React from 'react';
import NavbarComponent from '../../components/navbar/';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActionCreator from '../../actions/Authentication';

class NavbarCustom extends React.Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout() {
        this.props.authAction.logout();
    }

    render() {
        return (
            <NavbarComponent logout={this.logout} />
        );
    }

};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
    authAction: bindActionCreators(authActionCreator, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NavbarCustom);
