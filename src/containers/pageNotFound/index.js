import React from 'react';
import PageNotFoundComponent  from '../../components/pageNotfound/';

class PageNotFound extends React.Component {
    
    constructor(props){
        super(props);
    }

    render(){
        return (
            <PageNotFoundComponent />            
        )
    }
    
}

// const mapStateToProps = (state) => {};
// const mapDispatchToProps = (dispatch) => {};
//
export default PageNotFound;