import React from 'react';

import {BrowserRouter, Redirect, Route, Link,Switch, NavLink } from 'react-router-dom';
import AboutusComponent from '../../../components/home/aboutus/';
import Contact from '../aboutus/contact/';
// import Home from '../homepage/';
import Profile from '../aboutus/profile';
import Team from '../aboutus/team/';
import history from '../../../routers/history';

class Aboutus extends React.Component {
    
    constructor(props){
        super(props);        
    }
    
    render(){
        return (                                         
            <AboutusComponent />             
        )
    }
    
}

export default Aboutus;