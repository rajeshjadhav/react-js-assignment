import React from 'react';
import {BrowserRouter, Redirect, Route, Link,Switch, NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Aboutus from './aboutus/';
import Contact from './aboutus/contact/';
// import HomePage from './homepage';
import Profile from './aboutus/profile/';
import Team from './aboutus/team/';
import history from '../../routers/history';
import NavbarCustom from '../navbar/';

class Home extends React.Component {
    
    constructor(props){
        super(props);
    }

    componentDidMount(){
        if(localStorage.token==undefined){
            // <Redirect to='/login' />   
            history.push('/');
        }
    
    }

    render(){
        return (
            <div>                                 
                <NavbarCustom />
                <Switch>				
                    <Route exact path="/" component={Aboutus} />
                    <Route exact path="/aboutus" component={Aboutus}  />
                    <Route exact path="/aboutus/profile" component={Profile}  />
                    <Route exact path="/aboutus/team" component={Team} />
                    <Route exact path="/aboutus/contact" component={Contact} />                    
                </Switch>
            </div>
        )
    }
    
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Home));