import React from 'react';
import { Grid, Row, Col,FormControl,ControlLabel,FormGroup,HelpBlock, Button } from 'react-bootstrap';

const Login = (props) => (
    
    <Grid >
        <Row>
            <Col xs={12} sm={12} md={6} lg={6} className="wrapper">
                
                <form className="form-login ">       
                
                    <h2 className="form-login-heading">Login</h2>
                    
                    <FormControl type="text" 
                        className="form-control" 
                        name="email" 
                        placeholder="Email Address" 
                        required=""                        
                        onChange = { props.onChangeInput}
                    />     

                    <FormControl type="password"
                        className="form-control" 
                        name="password" 
                        placeholder="Password" 
                        required=""                        
                        onChange = { props.onChangeInput}
                    />    
                    <p>{ props.emailError +" "+ props.passwordError }</p> 
                    <br />
                    <Button bsStyle="primary" bsSize="large" block
                        onClick = { props.onHandleSubmit}>
                        Login
                    </Button>   
                    
                </form>
                
            </Col>            
        </Row>
    </Grid>
)

export default Login;