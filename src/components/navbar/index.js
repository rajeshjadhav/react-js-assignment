import React from 'react';
import {Navbar, NavItem, Nav, NavDropdown, MenuItem} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const NavbarCustom = (props) =>(
        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#brand">React JS </a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>              
              <NavDropdown eventKey={1} title="About US" id="basic-nav-dropdown">
                {/* <MenuItem eventKey={1.1}> */}
                  <li role="presentation">
                    <Link to="/aboutus/profile"> Profile </Link>
                  </li> 
                {/* </MenuItem> */}
                
                {/* <MenuItem eventKey={1.2}>                 */}
                  <li role="presentation">
                    <Link to="/aboutus/team"> Team </Link>
                  </li> 
                {/* </MenuItem> */}
                
                {/* <MenuItem eventKey={1.3}> */}
                  <li role="presentation">
                    <Link to="/aboutus/contact"> Contact </Link>
                  </li> 
                {/* </MenuItem>         */}
              </NavDropdown>
            </Nav>
            <Nav pullRight>
              <NavItem eventKey={1} onClick={ props.logout }>
                Logout
              </NavItem>      
            </Nav>
          </Navbar.Collapse>
        </Navbar>
)

export default NavbarCustom;

