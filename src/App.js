import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AppRouter from './routers/AppRouter';
import store from '../src/store/configureStore';

class App extends Component {
  render() {
    return (
        <AppRouter /> 	      
    );
  }
}

export default App;


