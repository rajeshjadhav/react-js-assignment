import { applyMiddleware ,createStore ,combineReducers } from 'redux';
import thunk from 'redux-thunk';
import authentication from '../reducers/authentication';

const middleware = applyMiddleware(thunk);

//create store
const store = createStore(
    combineReducers({
        'authentication' : authentication
    })
    ,middleware
);

export default store;
